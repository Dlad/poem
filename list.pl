#!/usr/bin/perl -w
use strict;
use LWP;
use DBI;
use Data::Dumper;
use utils;

$| = 1;

open URL, '<d.txt';
while(<URL>) {
	chomp;
	next if($_ eq '');
	my ($cat_url, $cat_name) = split("\t\t", $_);
	my $url = 'http://www.gushiwen.org' . $cat_url;

	my $ua = LWP::UserAgent->new;
	$ua->timeout(20);
	$ua = set_agent_header($ua);
	my $res = $ua->get($url);
	my $html = $res->content;

	#print $html . "\n";

	my @article = $html =~ /<a href="(\/GuShiWen_[^"]+)" target="_blank">([^<]+)<\/a>/g;
	while(@article) {
		my $link = shift @article;
		my $name = shift @article;

        my $now = time();
        my $dbh = utils->get_dbh();
        my $sql = "insert into article values('', '${cat_name}', '${name}', '${link}', ${now}, ${now});";
        $dbh->do($sql);

		my $line = "$link\t\t$name\t\t$cat_name\n";
		print $line;
	}
}
close URL;

sub set_agent_header
{
	my $ua = shift @_;
	
	my %fake_agent = (1=>'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.2; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)', 2=>'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.3029; Media Center PC 6.0; Tablet PC 2.0)', 3=>'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 6.0)', 4=>'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.0.3705; Media Center PC 3.1; Alexa Toolbar; .NET CLR 1.1.4322; .NET CLR 2.0.50727)', 5=>'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)', 6=>'Mozilla/4.0 (Mozilla/4.0; MSIE 7.0; Windows NT 5.1; FDM; SV1; .NET CLR 3.0.04506.30)', 7=>'Mozilla/4.0 (compatible; MSIE 6.1; Windows XP; .NET CLR 1.1.4322; .NET CLR 2.0.50727)', 8=>'Mozilla/5.0 (Windows NT 6.1; rv:10.0) Gecko/20100101 Firefox/10.0', 9=>'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.63 Safari/535.7');
	my $rand_key = int(rand(9));
	$rand_key++;

	$ua->default_header('User-Agent' => $fake_agent{$rand_key});
	$ua->default_header('Accept' => "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	$ua->default_header('Accept-Language' => "zh-cn,zh;q=0.5");
	$ua->default_header('Accept-Charset' => "GB2312,utf-8;q=0.7,*;q=0.7");
	$ua->default_header('Connection' => 'Keep-Alive');
	$ua->default_header('Host' => 'www.gushiwen.org');
	$ua->default_header('Referer' => "http://www.gushiwen.org/");

	return $ua;
}









