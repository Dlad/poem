package config;
use strict;

my $host = 'localhost';
my $user = 'root';
my $pw = '';

sub mysql_host {return $host;}
sub mysql_user {return $user;}
sub mysql_pw {return $pw;}

return 1;
