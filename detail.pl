#!/usr/bin/perl -w
use strict;
use utils;
use DBI;

my $dbh = utils->get_dbh();

my $sql;
my $offset = 0;
my $limit = 1;
while(1) {
    my $sth = $dbh->prepare("select id,url,content_raw from article limit $offset, $limit;");
    $sth->execute();
    my $ref = $sth->fetchrow_hashref();
    my $id = $ref->{'id'};
    my $url = 'http://www.gushiwen.org' . $ref->{'url'};
    
    last unless($id);
    $offset++;

    print "$id\n";
    #修复bug
    my $content_raw = $ref->{'content_raw'};
    next unless($content_raw eq '');
    
    my $page = utils->fetch_page($url, 'utf-8');
    $page =~ /作者：<\/span>[^<]+<(span|a)[^>]+><strong>([^<]+)/s;
    my $author = utils->trim($2);
    
    $page =~ /<div class="authorShow">(.+?<\/div>.+?)<\/div>/s;
    my $content = $1;
    #广告代码
    $content =~ s/<div id="ShowAd" class="ShowAd">.+?<\/div>//sg;
    #网站链接
    $content =~ s/<a href="http:\/\/www.gushiwen.org\/"[^>]+>.+?<\/a>//gs;
    #站内交叉索引链接
    $content =~ s/<\/?a[^>]+>//gs;
    #空p标签
    $content =~ s/<p[^>]+>\s+<\/p>//sg;

    $author = utils->add_slashes($author);
    $content = utils->add_slashes($content);
    $sql = "update article set author='${author}', content_raw='${content}' where id=${id};";
    $dbh->do($sql);
}

